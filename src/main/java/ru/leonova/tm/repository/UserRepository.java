package ru.leonova.tm.repository;

import ru.leonova.tm.entity.Task;
import ru.leonova.tm.entity.User;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserRepository {

    private Map<String, User> userMap = new LinkedHashMap();

    public void persist(User user) {
        userMap.put(user.getUserId(), user);
    }

    public User findOne(String userId) {
        return userMap.get(userId);
    }

    public Collection<User> findAll() {
        return userMap.values();
    }

    public void remove(User user) {
        userMap.remove(user.getUserId());
    }

    public void removeAll() {
        userMap.clear();
    }

}
