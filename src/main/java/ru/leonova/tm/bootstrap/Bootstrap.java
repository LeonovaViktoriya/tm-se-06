package ru.leonova.tm.bootstrap;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.repository.ProjectRepository;
import ru.leonova.tm.repository.TaskRepository;
import ru.leonova.tm.repository.UserRepository;
import ru.leonova.tm.service.ProjectService;
import ru.leonova.tm.service.TaskService;
import ru.leonova.tm.service.UserService;

import java.util.*;

public class Bootstrap {

    private final Scanner scanner = new Scanner(System.in);
    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private UserRepository userRepository = new UserRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskService taskService = new TaskService(taskRepository);
    private UserService userService = new UserService(userRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public Bootstrap() {
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public UserService getUserService() {
        return userService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    private void registry(AbstractCommand command) throws Exception {
        String cliCommand = command.getName();
        String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty()) throw new Exception("It is not enumerated");
        if (cliDescription == null || cliDescription.isEmpty()) throw new Exception("Not description");
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    private boolean isCorrectCommand(String command) {
        for (AbstractCommand c:commands.values()) {
            if (c.getName().equals(command)) {
                return true;
            }
        }
        return false;
    }

    private void registry(Class... classes) throws Exception {
        for (Class clazz : classes) {
            if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
            Object command = clazz.newInstance();
            AbstractCommand abstractCommand = (AbstractCommand) command;
            registry(abstractCommand);
        }
    }

    public void init(Class... classes) throws Exception {
        if (classes == null || classes.length == 0) throw new Exception("Empty list");
        registry(classes);
        initAdmin();
        start();
    }

    private void initAdmin() {
        try {
            User admin = new User("admin", userService.md5Apache("admin"));
            admin.setRoleType(RoleType.ADMIN.getRole());
            userService.adminRegistration(admin);
        } catch (Exception e) {
            System.out.println("Something went wrong in initAdmin!");
            e.printStackTrace();
        }
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command;
        do {
            command = scanner.nextLine();
            execute(command);
        } while (!command.equals("exit"));
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty() || !isCorrectCommand(command)) return;
        final AbstractCommand abstractCommand = commands.get(command);
        final boolean secureCheck = !abstractCommand.secure() || (abstractCommand.secure() && userService.isAuth());
        if (secureCheck || userService.getCurrentUser() != null) {
            abstractCommand.execute();
        } else {
            System.out.println("Log in for this command");
        }

    }

}

