package ru.leonova.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.repository.UserRepository;

import java.util.Collection;
import java.util.Map;

public class UserService {

    private UserRepository userRepository;
    private User currentUser;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    public User getCurrentUser() {
        return currentUser;
    }
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void createUser(User user) {
        if (user == null) return;
        user.setRoleType(RoleType.USER.getRole());
        userRepository.persist(user);
    }

    public User authorizationUser(String login, String password) {
        if (login.isEmpty() || password.isEmpty()) return null;
        Collection<User> userCollection = userRepository.findAll();
        for (User user : userCollection) {
            if (user.getLogin().equals(login) & user.getPassword().equals(md5Apache(password))) {
                currentUser = user;
                return currentUser;
            }
        }
        return null;
    }

    public boolean isAuth() {
        return currentUser != null;
    }

    public User getUserById(String userId) {
        return userRepository.findOne(userId);
    }

    public String md5Apache(String password) {
        return DigestUtils.md5Hex(password);
    }

    public Collection<User> getListUsers() {
        return userRepository.findAll();
    }

    public void adminRegistration(User admin) {
        userRepository.persist(admin);
    }
}
