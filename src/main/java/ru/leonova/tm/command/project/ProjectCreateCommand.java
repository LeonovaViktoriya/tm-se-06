package ru.leonova.tm.command.project;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "create-p";
    }

    @Override
    public String getDescription() {
        return "Create project";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        if (!bootstrap.getUserService().isAuth()){
            System.out.println("You are not authorized");
            return;
        }
        System.out.print("[CREATE PROJECT]\nINTER NAME: \n");
        Project project = new Project(getScanner().nextLine());
        bootstrap.getProjectService().createProject(project);
        System.out.println("Project created");
    }
}
