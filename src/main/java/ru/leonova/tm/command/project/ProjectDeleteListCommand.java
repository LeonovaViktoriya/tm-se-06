package ru.leonova.tm.command.project;

import ru.leonova.tm.command.AbstractCommand;

public class ProjectDeleteListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-all-p";
    }

    @Override
    public String getDescription() {
        return "Delete list projects";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        if (!bootstrap.getUserService().isAuth()){
            System.out.println("You are not authorized");
            return;
        }
        bootstrap.getTaskService().deleteAllTask();
        bootstrap.getProjectService().deleteAllProject();
        System.out.println("All projects remove");
    }
}
