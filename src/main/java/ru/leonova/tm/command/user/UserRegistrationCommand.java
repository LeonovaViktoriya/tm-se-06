package ru.leonova.tm.command.user;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;

public class UserRegistrationCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "reg";
    }

    @Override
    public String getDescription() {
        return "Registration user".toUpperCase();
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute(){
        System.out.println("["+getDescription()+"]\nEnter login:");
        String login = getScanner().nextLine();
        System.out.println("Enter password:");
        String password = getScanner().nextLine();
        password = bootstrap.getUserService().md5Apache(password);
        User user = new User(login, password);
        bootstrap.getUserService().createUser(user);
        System.out.println("User is registered");
    }
}
