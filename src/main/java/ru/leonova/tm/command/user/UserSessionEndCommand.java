package ru.leonova.tm.command.user;

import ru.leonova.tm.command.AbstractCommand;

public class UserSessionEndCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "session-end";
    }

    @Override
    public String getDescription() {
        return "Завершение сеанса пользователя".toUpperCase();
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        System.out.println("["+getDescription()+"]");
        bootstrap.getUserService().setCurrentUser(null);
    }
}
