package ru.leonova.tm.command.user;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Collection;

public class UserShowListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "list-u";
    }

    @Override
    public String getDescription() {
        return "SHOW LIST USERS";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute(){
        System.out.println("["+getDescription()+"]");
        if(bootstrap.getUserService().getCurrentUser().getRoleType().equals(RoleType.ADMIN.getRole())){
            Collection<User> userCollection = bootstrap.getUserService().getListUsers();
            int i = 0;
            for (User user : userCollection) {
                i++;
                System.out.println(i+". Login: " + user.getLogin() + ", Password: " + user.getPassword() + ", Id: " + user.getUserId() + ", Role type: " + user.getRoleType());
            }
        }else {
            System.out.println("You are not admin");
        }
    }
}
