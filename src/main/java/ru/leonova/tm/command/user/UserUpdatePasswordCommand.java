package ru.leonova.tm.command.user;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;

public class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "up-p";
    }

    @Override
    public String getDescription() {
        return "Update user password".toUpperCase();
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        System.out.println("["+getDescription()+"]");
        User currentUser = bootstrap.getUserService().getCurrentUser();
        if(currentUser==null || currentUser.getPassword().isEmpty())return;
        System.out.println("Enter new password");
        String password = getScanner().nextLine();
        password = bootstrap.getUserService().md5Apache(password);
        currentUser.setPassword(password);
        System.out.println("Password is updated");
    }
}
