package ru.leonova.tm.command.user;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;

public class UserUpdateLoginCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "up-l";
    }

    @Override
    public String getDescription() {
        return "Update login".toUpperCase();
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        System.out.println("["+getDescription()+"]");
        bootstrap.getUserService().isAuth();
        User currentUser = bootstrap.getUserService().getCurrentUser();
        if(currentUser.getLogin().isEmpty()) return;
        System.out.println("Enter new login");
        currentUser.setLogin(getScanner().nextLine());
        System.out.println("Login is updated");
    }
}
