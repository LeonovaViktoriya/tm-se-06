package ru.leonova.tm.command.user;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Collection;

public class UserUpdateRoleCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "up-role";
    }

    @Override
    public String getDescription() {
        return "Admin can edit role user to admin".toUpperCase();
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        if (!bootstrap.getUserService().getCurrentUser().getRoleType().equals(RoleType.ADMIN.getRole())) {
            System.out.println("This option available just for admin");
            return;
        }
        System.out.println("[" + getDescription() + "]");
        System.out.println("[List user:]");
        Collection<User> userCollection = bootstrap.getUserService().getListUsers();
        int i = 0;
        for (User user : userCollection) {
            i++;
            System.out.println(i+". Login: " + user.getLogin() + ", Password: " + user.getPassword() + ", Id: " + user.getUserId() + ", Role type: " + user.getRoleType());
        }
        System.out.println("Enter id user");
        String userId = getScanner().nextLine();
        User user = bootstrap.getUserService().getUserById(userId);
        if (user == null) {
            System.out.println("User with this id not found");
        } else {
            user.setRoleType(RoleType.ADMIN.getRole());
            System.out.println("Role user was update to admin");
        }

    }
}
