package ru.leonova.tm.command;

import ru.leonova.tm.bootstrap.Bootstrap;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Scanner;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract boolean secure();

    public abstract void execute() throws Exception;

    public Bootstrap getBootstrap() {
        return bootstrap;
    }

    protected Scanner getScanner(){
        return new Scanner(System.in);
    }

    public  RoleType[] roles() {
        return null;
    }

}
