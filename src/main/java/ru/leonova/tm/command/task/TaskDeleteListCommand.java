package ru.leonova.tm.command.task;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.enumerated.RoleType;

public class TaskDeleteListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-all-t";
    }

    @Override
    public String getDescription() {
        return "Delete list tasks";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }

    @Override
    public void execute(){
        System.out.println("["+getDescription().toUpperCase()+"]");
        bootstrap.getTaskService().deleteAllTask();
        System.out.println("All tasks remove");
    }
}
